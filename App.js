/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ContactListScreen from './src/screens/ContactListScreen';
import { Provider } from 'react-redux';
import configureStore from './src/config/store/configureStore';
import DetailContactScreen from './src/screens/DetailContactScreen';
import AddContact from './src/screens/AddContact';



const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <Provider store={configureStore}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='ContactListScreen'>
          <Stack.Screen
            name='ContactListScreen'
            component={ContactListScreen}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name='DetailContactScreen'
            component={DetailContactScreen}
          />
          <Stack.Screen
            name='AddContact'
            component={AddContact}
            options={{ headerShown: false }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
