/* eslint-disable prettier/prettier */
import { createStore, combineReducers } from 'redux';
import getContactListReducer from '../../redux/reducers/getContactListReducer';
import { applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
  contactList: getContactListReducer,
});

const configureStore = createStore(
  rootReducer,
  applyMiddleware(thunk),
);

export default configureStore;
