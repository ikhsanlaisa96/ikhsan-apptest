/* eslint-disable prettier/prettier */
import { Dimensions } from 'react-native';

const getResponsiveValue = (value: number) => {
  const { height } = Dimensions.get('window');
  if (height <= 667) {
    // small size phone
    return value * 0.8;
  } else if (height > 667 && height <= 736) {
    // medium size phone
    return value * 1;
  } else {
    //large size phone
    return value * 1;
  }
};

const handleResponsiveValue = (arrayOfValues: [number, number, number]) => {
  const { height } = Dimensions.get('window');
  if (height < 667) {
    // small size phone
    return arrayOfValues[0];
  } else if (height >= 667 && height <= 736) {
    // medium size phone
    return arrayOfValues[1];
  } else {
    return arrayOfValues[2];
  }
};

export {
  handleResponsiveValue,
  getResponsiveValue,
};