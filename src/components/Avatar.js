/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { Image, StyleSheet } from 'react-native';

const Avatar = (props) => {
  const setImage = (data) => {
    // const image = '';
    if (data === 'N/A') {
      return { uri: 'https://thumbs.dreamstime.com/b/default-avatar-profile-vector-user-profile-default-avatar-profile-vector-user-profile-profile-179376714.jpg' }
    }
    return { uri: data };
  };

  return (
    <Image
      source={setImage(props.image)}
      style={{ ...styles.image, ...props.styles }}
    />
  )
};

const styles = StyleSheet.create({
  image: {
    marginTop: 5,
    marginBottom: 5,
    overflow: 'hidden',
  },
});

export default Avatar;
