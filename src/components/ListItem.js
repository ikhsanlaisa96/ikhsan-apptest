/* eslint-disable prettier/prettier */
import React from 'react';
import {
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { getResponsiveValue, handleResponsiveValue } from '../config/common';
import Avatar from './Avatar';

const Item = ({ firstName, lastName, image, navigation, id }) => (
  <TouchableOpacity
    style={styles.itemContainer}
    onPress={() => {
      navigation.navigate('DetailContactScreen', { userId: id });
    }}
  >
    <View style={styles.item}>
      <Avatar
        image={image}
        styles={{
          height: handleResponsiveValue([30, 40, 50]),
          width: handleResponsiveValue([30, 40, 50]),
          borderRadius: handleResponsiveValue([30, 40, 50]),
          marginRight: handleResponsiveValue([6, 8, 10]),
        }}
      />
      <View style={styles.titleWrapper}>
        <Text style={styles.title}>{firstName}</Text>
        <Text style={styles.details}>{lastName}</Text>
      </View>
    </View>
  </TouchableOpacity>
);

const List = (props) => {
  const renderItem = ({ item }) => {
    // when no input, show all
    if (props.searchPhrase === '') {
      return <Item firstName={item.firstName} lastName={item.lastName} image={item.photo} navigation={props.navigation} id={item.id} />;
    }
    // filter of the name
    if (item.name.toUpperCase().includes(props.searchPhrase.toUpperCase().trim().replace(/\s/g, ''))) {
      return <Item firstName={item.firstName} lastName={item.lastName} />;
    }
    // filter of the description
    if (item.details.toUpperCase().includes(props.searchPhrase.toUpperCase().trim().replace(/\s/g, ''))) {
      return <Item firstName={item.firstName} lastName={item.lastName} />;
    }
  };

  return (
    <SafeAreaView style={styles.list__container}>
      <View
        onStartShouldSetResponder={() => {
          props.setClicked(false);
        }}
      >
        <FlatList
          data={props.data}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  list__container: {
    margin: 10,
    height: '85%',
    width: '100%',
  },
  itemContainer: {
    backgroundColor: 'lightblue',
    borderRadius: 25,
    marginLeft: handleResponsiveValue([8, 12, 16]),
    marginRight: handleResponsiveValue([8, 12, 16]),
    marginTop: handleResponsiveValue([4, 6, 8]),
    marginBottom: handleResponsiveValue([4, 6, 8]),
    padding: handleResponsiveValue([8, 12, 16]),
  },
  item: {
    flexDirection: 'row',
    borderBottomWidth: 2,
    borderBottomColor: 'black',
  },
  titleWrapper: {
    flexDirection: 'column',
    alignContent: 'center',
  },
  title: {
    fontSize: handleResponsiveValue([12, 16, 20]),
    fontWeight: 'bold',
    marginBottom: 5,
    fontStyle: 'italic',
  },
});

export default List;
