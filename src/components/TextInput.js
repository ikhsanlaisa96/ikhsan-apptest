/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import { StyleSheet, TextInput } from 'react-native';
import { handleResponsiveValue } from '../config/common';

const InputText = props => {
  const [text, onChangeText] = useState(props.value);

  // useEffect(() => {
  //   onChangeText(props.value);
  // });
  return (
    <TextInput
      style={styles.input}
      onChangeText={onChangeText}
      value={text}
    />
  );
};

const styles = StyleSheet.create({
  input: {
    height: handleResponsiveValue([32, 36, 40]),
    margin: handleResponsiveValue([8, 10, 12]),
    borderWidth: 1,
    padding: handleResponsiveValue([6, 8, 10]),
    // flex: 1,
    borderRadius: 40,
    width: '100%',
  },
});

export default InputText;
