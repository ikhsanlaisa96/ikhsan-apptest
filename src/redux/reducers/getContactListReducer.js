/* eslint-disable prettier/prettier */
import {
  CREATE_CONTACT,
  DELETE_CONTACT,
  GET_CONTACT,
  GET_CONTACT_BY_ID,
  UPDATE_CONTACT,
} from '../../config/constant';

const initialState = {
  contactList: [],
};

const getContactListReducer = (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_CONTACT:
      return {
        ...state,
        contactList: payload,
      };
    case GET_CONTACT_BY_ID:
      return payload;
    case CREATE_CONTACT:
      return [...state, payload];
    case UPDATE_CONTACT:
      return state.map((data) => {
        if (data.id === payload.id) {
          return {
            ...data,
            ...payload,
          };
        }
      });
    case DELETE_CONTACT:
      return state.filter(({ id }) => id !== payload);
    default:
      return state;
  }
};

export default getContactListReducer;

