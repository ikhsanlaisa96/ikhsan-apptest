/* eslint-disable prettier/prettier */
import axios from 'axios';
import { CREATE_CONTACT, DELETE_CONTACT, GET_CONTACT, GET_CONTACT_BY_ID, UPDATE_CONTACT } from '../../config/constant';

export function setContactData(actionType, contactList) {
  return {
    type: actionType,
    payload: contactList,
  };
};

export function contactListAction() {
  return async (dispatch) => {
    try {
      const apiReq = await axios.get('https://simple-contact-crud.herokuapp.com/contact');
      await dispatch(setContactData(GET_CONTACT, apiReq.data.data));
      return apiReq;
    } catch (e) {
      console.error(e);
    }
  };
};

export function getContactListById(id) {
  return async (dispatch) => {
    try {
      const apiReq = await axios.get(`https://simple-contact-crud.herokuapp.com/contact/${id}`);
      await dispatch(setContactData(GET_CONTACT_BY_ID, apiReq.data.data));
      console.log('init brokuuu', apiReq);
      return apiReq;
    } catch (e) {
      console.log(e);
    }
  };
};

export function createContact(params) {
  return async (dispatch) => {
    try {
      const apiReq = await axios.post('https://simple-contact-crud.herokuapp.com/contact', params);
      await dispatch(setContactData(CREATE_CONTACT, params));
      return apiReq;
    } catch (e) {
      console.log(e);
    }
  };
};

export function updateContact(id, params) {
  return async (dispatch) => {
    try {
      const apiReq = await axios.put(`https://simple-contact-crud.herokuapp.com/contact/${id}`, params);
      await dispatch(setContactData(UPDATE_CONTACT, params));
      return apiReq;
    } catch (e) {
      console.log(e);
    }
  };
};

export function deleteContact(id) {
  return async (dispatch) => {
    try {
      const apiReq = await axios.delete(`https://simple-contact-crud.herokuapp.com/contact/${id}`);
      await dispatch(setContactData(DELETE_CONTACT, id));
      return apiReq;
    } catch (e) {
      console.log(e);
    }
  };
};
