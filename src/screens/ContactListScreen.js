/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import { SafeAreaView, StyleSheet, Text, ActivityIndicator, View, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import List from '../components/ListItem';
import { handleResponsiveValue } from '../config/common';
import * as contactListAction from '../redux/actions/contactListAction';

const ContactListScreen = (props) => {
  const [searchPhrase, setSearchPhrase] = useState('');
  const [data, setData] = useState();
  const { actions, contactList, navigation } = props;

  useEffect(() => {
    actions.contactListAction();
    setData(contactList);
  }, [actions, contactList]);

  return (
    <SafeAreaView style={styles.root}>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('AddContact');
        }}
      >
        <Text style={styles.addButton}>Add Contact</Text>
      </TouchableOpacity>
      <List
        searchPhrase={searchPhrase}
        data={data}
        navigation={navigation}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  root: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  addButton: {
    alignSelf: 'flex-end',
    fontSize: handleResponsiveValue([15, 20, 25]),
    fontWeight: 'bold',

  }
});

const mapStateToProps = state => ({
  contactList: state.contactList.contactList,
});

const ActionCreators = Object.assign(
  {},
  contactListAction,
);

// connect
const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ContactListScreen);
