/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import { ActivityIndicator, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import Avatar from '../components/Avatar';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { handleResponsiveValue } from '../config/common';
import InputText from '../components/TextInput';
import * as getContactLisyById from '../redux/actions/contactListAction';


const DetailContactScreen = (props) => {
  const { userId } = props.route.params;
  const { actions, contactData } = props;
  const [data, setData] = useState();

  useEffect(() => {
    actions.getContactListById(userId);
    setData(contactData);
  }, [actions, userId, contactData,]);

  return (
    <SafeAreaView>
      <View style={styles.container}>
        {!data ? (
          <ActivityIndicator size='large' />
        ) : (
          <View>
            <Avatar
              image={data.photo}
              styles={{
                height: handleResponsiveValue([70, 80, 90]),
                width: handleResponsiveValue([70, 80, 90]),
                borderRadius: handleResponsiveValue([70, 80, 90]),
              }}
            />
            <InputText value={data.firstName} />
            <InputText value={data.lastName} />
            <InputText value={data.age} />
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: handleResponsiveValue([8, 12, 16]),
  }
});

const mapStateToProps = state => ({
  contactData: state.contactList,
});

const ActionCreators = Object.assign(
  {},
  getContactLisyById,
);

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(ActionCreators, dispatch),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailContactScreen);
